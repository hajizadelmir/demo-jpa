package az.ingress.ms9;

import az.ingress.ms9.dto.StudentDto;
import az.ingress.ms9.model.Address;
import az.ingress.ms9.model.Student;
import az.ingress.ms9.repository.StudentRepository;
import az.ingress.ms9.service.StudentServiceImpl;
import com.speedment.jpastreamer.application.JPAStreamer;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import javax.persistence.EntityManagerFactory;
import java.util.List;
import java.util.Set;

@SpringBootApplication
@EnableConfigurationProperties
@RequiredArgsConstructor

public class Ms9Application implements CommandLineRunner {
    private final EntityManagerFactory entityManagerFactory;
    private final StudentServiceImpl service;
    private final StudentRepository repository;

    public static void main(String[] args) {
        SpringApplication.run(Ms9Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        List<StudentDto> list;
        //NativeQuery
        list = service.getByEndTwoCharactersAndEqualsCity2();
        list.forEach(System.out::println);

        //JPQL
        list = service.getByEndTwoCharactersAndEqualsCity();
        list.forEach(System.out::println);




//        Student student = new Student();
//        student.setSurname("Memmedov");
//
//        Student student2 = new Student();
//        student2.setSurname("Imanov");
//
//        Student student3 = new Student();
//        student3.setSurname("Aliyev");
//
//        Student student4 = new Student();
//        student4.setSurname("Hajiyev");
//
//        Student student5 = new Student();
//        student5.setSurname("Imranov");
//
//        Address address = new Address();
//        address.setName("Baku");
//
//        Address address2 = new Address();
//        address2.setName("Ganja");
//
//        Address address3 = new Address();
//        address3.setName("Shaki");
//
//        Address address4 = new Address();
//        address4.setName("Baku");
//
//        Address address5 = new Address();
//        address5.setName("Absheron");
//        Set<Address> addressSet = Set.of(address, address2, address3, address4, address5);
//        Set<Student> studentsList = Set.of(student, student2, student3, student4, student5);
//        repository.saveAll(studentsList);


    }
}
