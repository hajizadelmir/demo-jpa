package az.ingress.ms9.repository;

import az.ingress.ms9.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student, Long> {

    @Query(value = "SELECT s FROM Student s" +
            "JOIN s.address a" +
            "WHERE s.surname LIKE '%ov' AND a.name='Baku' ")
    List<Student> getByEndTwoCharactersAndEqualsCity();


    @Query(value = "SELECT * FROM student "+
    "INNER JOIN address ON student.address_id=address.id"+
    "WHERE student.surname LIKE '%ov' AND address.name='BAKU'",nativeQuery = true)
    List<Student> getByEndTwoCharactersAndEqualsCity2();




    }
