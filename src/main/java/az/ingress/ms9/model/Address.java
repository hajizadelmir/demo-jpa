package az.ingress.ms9.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data

public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @OneToOne(mappedBy = "address")
    private Student student;

}
