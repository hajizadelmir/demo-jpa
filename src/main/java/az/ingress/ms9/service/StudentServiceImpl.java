package az.ingress.ms9.service;

import az.ingress.ms9.dto.StudentDto;
import az.ingress.ms9.model.Student;
import az.ingress.ms9.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;
@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {
    private final StudentRepository repository;
    private final ModelMapper mapper;

    @Override
    public List<StudentDto> getByEndTwoCharactersAndEqualsCity() {
        List<Student> list = repository.getByEndTwoCharactersAndEqualsCity();
        List<StudentDto> studentDtoList = list.stream().map(student -> mapper.map(student,StudentDto.class)).collect(Collectors.toList());
        return studentDtoList;
    }

    @Override
    public List<StudentDto> getByEndTwoCharactersAndEqualsCity2() {
        List<Student> list = repository.getByEndTwoCharactersAndEqualsCity2();
        List<StudentDto> studentDtoList = list.stream().map(student -> mapper.map(student,StudentDto.class)).collect(Collectors.toList());
        return studentDtoList;

    }

    @Override
    public StudentDto getStudentById(Long id) {
        Student byId = repository.getById(id);
        StudentDto dto = mapper.map(byId, StudentDto.class);
        return dto;
    }

    @Override
    public StudentDto createStudent(StudentDto dto) {
        Student student = mapper.map(dto, Student.class);
        Student save = repository.save(student);
        return mapper.map(save, StudentDto.class);

    }

    @Override
    public void deleteStudent(Long id) {
        repository.deleteById(id);
    }

    @Override
    public StudentDto updateStudent(StudentDto dto) {
        Student student = repository.findById(dto.getId()).orElseThrow((() -> new RuntimeException("Student Not Found")));
        student.setSurname(dto.getFirstName());
        Student save = repository.save(student);
        return mapper.map(save,StudentDto.class);
    }

    @Override
    public List<StudentDto> findAllStudent() {
        List<Student>all=repository.findAll();
        List<StudentDto> e=all.stream().map(a->mapper.map(a,StudentDto.class)).collect(Collectors.toList());
        return e ;
    }
}
